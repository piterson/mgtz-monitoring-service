import {Component, OnDestroy, OnInit} from '@angular/core';
import {WorkersService} from "./workers.service";
import {WorkerStats} from "./models/worker";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  workers: WorkerStats[] = [];
  displayedColumns: string[] = ['name', 'status', 'lastActivity', 'cpuUsage', 'tasks', 'metricXY'];
  private timeOut;

  constructor(private service: WorkersService){}

  ngOnInit(): void {
    this.fetchWorkers()
  }

  private fetchWorkers() {
    this.service.getWorkers().subscribe(result => {
      this.workers = result;
      this.timeOut = setTimeout(() => this.fetchWorkers(), 10000);
    });
  }

  ngOnDestroy(): void {
    clearTimeout(this.timeOut);
  }
}
