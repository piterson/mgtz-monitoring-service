import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {WorkerStats} from "./models/worker";
import {catchError, tap} from "rxjs/operators";

const HOST = 'http://localhost:8000';

@Injectable()
export class WorkersService {

  constructor(private http: HttpClient) { }

  /** GET stats from the server */
  getWorkers (): Observable<WorkerStats[]> {
    return this.http.get<WorkerStats[]>(HOST + '/api/workers')
      .pipe(
        tap(_ => console.log('fetched data')),
        catchError(this.handleError('getHeroes', []))
      );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
