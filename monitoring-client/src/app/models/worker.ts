export interface Worker {
  id: number;
  name: string;
}

export interface Stats {
  created: string;
  params: Params;
  online: boolean;
}

export interface Params {
  metricXY: number;
  cpuUsage: number;
  tasks: number;
}

export type WorkerStats = Worker & Stats;
