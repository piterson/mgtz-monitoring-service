import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {WorkersService} from "./workers.service";
import {HttpClientModule} from "@angular/common/http";
import {MatTableModule} from "@angular/material";
import {TimeAgoPipe} from "time-ago-pipe";

@NgModule({
  declarations: [
    AppComponent,
    TimeAgoPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MatTableModule
  ],
  providers: [WorkersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
