'use strict';

const keyValues = Symbol('values');

class Settings {
	constructor(settings = {}) {
		this[keyValues] = Object.keys(settings).reduce((result, key) => {
			let value = process.env.hasOwnProperty(key) ? process.env[key] : settings[key];
			if(value === undefined) {
				throw new Error(`Value is not specified for ${key}`);
			}
			if(isFinite(value)) {
				value = Number.isInteger(Number(value)) ? Number.parseInt(value) : Number.parseFloat(value);
			}
			result[key] = value;
			return result;
		}, {});
	}

	getValue(key) {
		return this[keyValues][key];
	}

	toString() {
		return Object.keys(this[keyValues]).map(key => `${key} = ${this[keyValues][key]}`).join('; ');
	}
}
module.exports = Settings;
