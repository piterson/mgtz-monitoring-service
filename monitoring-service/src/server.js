const express = require('express');
const compress = require('compression');
const cors = require('cors')
const bodyParser = require('body-parser');

const {ServerSettings} = require('./settings');
const settings = new ServerSettings();

var corsOptions = {
    origin: settings.clientDomain,
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

function startServer(routers, errorHandler) {
    if(!routers) {
        throw new Error('Router is not specified');
    }
    if(!Array.isArray(routers)) {
        routers = [routers];
    }
    const app = express();

    app.disable('x-powered-by');
    app.use(compress());
    app.use(bodyParser.text({limit: '50mb'}));
    app.use(bodyParser.json({limit: '50mb'}));
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(cors(corsOptions));

    routers.forEach(r => app.use(r.prefix || '', r));

    if(errorHandler) {
        app.use((err, req, res, next) => errorHandler(err, res, next));
    }

    return new Promise(resolve => {
        app.listen(settings.listenPort, settings.listenHost, () => resolve());
    });
}

module.exports = startServer;
