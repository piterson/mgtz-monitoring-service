'use strict';

const pg = require('pg');


const keyPool = Symbol('pool');

class Db {
    constructor(dbSettings) {
        if(!dbSettings) {
            throw new Error('database settings are not specified');
        }
        this[keyPool] = new pg.Pool({
            database : dbSettings.db,
            user     : dbSettings.user,
            password : dbSettings.password,
            host     : dbSettings.host,
            port     : dbSettings.port,
            max      : dbSettings.connectionPoolSize,
        });

        const connString = `pg://${dbSettings.user}:${dbSettings.password && '***'}@${dbSettings.host}:${dbSettings.port}/${dbSettings.db}?pool=${dbSettings.connectionPoolSize}`;
        console.log(`New connection pool to PostgreSQL DB has been created successfully\nConnection string: ${connString}`);
    }

    connect() {
        return this[keyPool].connect();
    }

    disconnect() {
        this[keyPool].disconnect();
    }

    query(sql) {
        return this.connect()
            .then(client => {
                return client.query(sql.text, sql.values)
                    .then(result => {
                        client.release();
                        switch(result.command) {
                            case 'UPDATE':
                            case 'DELETE':
                                return { affectedRows: result.rowCount };

                            default:
                                return result.rows;
                        }
                    })
                    .catch(err => {
                        client.release();
                        return Promise.reject(err);
                    });
            });
    }
}

module.exports = Db;
