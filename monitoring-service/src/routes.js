const express = require('express');
const Router = express.Router;
const WorkersService = require('./controllers/worker-stat');


function AppRouter() {
    const router = new Router();
    const workers = new WorkersService();

    //workers
    router.post('/api/worker/stat', workers.pushMetrics.bind(workers));
    router.get('/api/workers', workers.getWorkers.bind(workers));
    //router.get('/api/worker/:workerId/log', workers.getWorkerLog.bind(workers));

    return router;
}

module.exports = AppRouter;
