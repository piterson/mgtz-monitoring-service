"use strict";

const {WorkersRepo, LogsRepo, DataRepo} = require('./worker-stat-db');
const fromEntries = require('fromentries')

const keyWorkersRepo = Symbol()
const keyLogsRepo = Symbol()
const keyDataRepo = Symbol()

class WorkersService {
    constructor() {
        this[keyWorkersRepo] = new WorkersRepo();
        this[keyLogsRepo] = new LogsRepo();
        this[keyDataRepo] = new DataRepo();
    }

    async pushMetrics(req, res, errorHandler) {
        const workersRepo = this[keyWorkersRepo];
        const logsRepo = this[keyLogsRepo];
        const dataRepo = this[keyDataRepo];

        try {
            const params = req.body;
            const workerName = params.workerId;
            delete params.workerId;

            let worker = await workersRepo.getByName(workerName);
            if (!worker) {
                worker = await workersRepo.create({name: workerName});
            }
            const log = await logsRepo.create(worker.id);
            await workersRepo.update(worker.id, log.id);
            const data = Object.entries(params).map(([paramName, paramValue]) => dataRepo.create(log.id, {paramName, paramValue}));
            await Promise.all(data);

            res.send(log);
        } catch (err) {
            errorHandler(err);
        }
    }

    async getWorkers(req, res, errorHandler) {
        const workersRepo = this[keyWorkersRepo];

        const paramsToObject = (params) => fromEntries(params.split(',').map(v => v.split('=')));
        const minTime = Date.now() - 60000;
        try{
            const workers = await workersRepo.findAll();
            const normalized = workers.map(w => ({...w, params: paramsToObject(w.params), online: w.created > minTime}));
            res.send(normalized);
        } catch (err) {
            errorHandler(err);
        }
    }
}

module.exports = WorkersService;
