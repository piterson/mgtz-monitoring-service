'use strict';

const squel = require('squel').useFlavour('postgres');
const S = require('string');
const _ = require('lodash');
const {DbSettings} = require('../settings');
const Db = require('../db-postgresql');

const WORKERS_TABLE = 'public.workers';
const LOGS_TABLE = 'public.worker_logs';
const DATA_TABLE = 'public.worker_log_data';

squel.registerValueHandler(Date, v => v.toISOString());
squel.registerValueHandler(Object, v => JSON.stringify(v));

class WorkersRepo {
    constructor() {
        const dbSettings = new DbSettings();
        this.db = new Db(dbSettings);
    }

    create(entity) {
        const db = this.db;

        const row = entityToRow(entity);

        let sql = squel.insert()
            .into(WORKERS_TABLE)
            .setFields(row) // sorry, there no fields validation
            .returning('*')
            .toParam();

        return db.query(sql).then(rows => {
            if (rows.length === 0) {
                throw new Error('Insertion to db failed');
            }
            return rowToEntity(rows[0]);
        });
    }

    update(workerId, lastLogId) {
        const db = this.db;

        const row = entityToRow({lastLogId});

        const sql = squel.update()
            .table(WORKERS_TABLE)
            .setFields(row)
            .where('id = ?', workerId)
            .returning('id')
            .toParam();

        return db.query(sql).then(rows => {
            if (rows.length === 0) {
                throw new Error('Insertion to db failed');
            }
            return rowToEntity(rows[0]);
        });
    }


    getByName(workerName) {
        const db = this.db;

        let sql = squel.select()
            .from(WORKERS_TABLE)
            .where('name = ?', workerName)
            .limit(1)
            .toParam();

        return db.query(sql).then(rows => {
            return rows.length > 0 && rowToEntity(rows[0]);
        });
    }

    findAll() {
        const db = this.db;

        const sql = squel.select()
            .from(WORKERS_TABLE, 'w')
            .field('w.*')
            .field('l.created')
            .field(`string_agg(concat(d.param_name, '=', d.param_value), ',')`, 'params')
            .left_join(LOGS_TABLE, 'l', 'l.id=w.last_log_id')
            .left_join(DATA_TABLE, 'd', 'd.log_id=w.last_log_id')
            .group('w.id').group('w.name').group('l.created').group('w.last_log_id')
            .toParam();

        return db.query(sql).then(r => r.map(rowToEntity));
    }
}

class LogsRepo {
    constructor() {
        const dbSettings = new DbSettings();
        this.db = new Db(dbSettings);
    }

    create(workerId) {
        const db = this.db;

        const row = entityToRow({workerId});

        let sql = squel.insert()
            .into(LOGS_TABLE)
            .setFields(row) // sorry, there no fields validation
            .returning('*')
            .toParam();

        return db.query(sql).then(rows => {
            if (rows.length === 0) {
                throw new Error('Insertion to db failed');
            }
            return rowToEntity(rows[0]);
        });
    }
}

class DataRepo {
    constructor() {
        const dbSettings = new DbSettings();
        this.db = new Db(dbSettings);
    }

    create(logId, entity) {
        const db = this.db;

        entity.logId = logId;
        const row = entityToRow(entity);

        let sql = squel.insert()
            .into(DATA_TABLE)
            .setFields(row) // sorry, there no fields validation
            .returning('*')
            .toParam();

        return db.query(sql).then(rows => {
            if (rows.length === 0) {
                throw new Error('Insertion to db failed');
            }
            return rowToEntity(rows[0]);
        });
    }
}

function entityToRow(entity) {
    return Object.keys(entity)
        .reduce((result, key) => {
            let value = entity[key];
            if(Array.isArray(value)) {
                value = `{${value.join()}}`;
            }
            result[S(key).underscore()] = value;
            return result;
        }, {});
}

function rowToEntity(row) {
    return _.mapKeys(row, (v, k) => S(k).camelize());
}

exports.WorkersRepo = WorkersRepo;
exports.LogsRepo = LogsRepo;
exports.DataRepo = DataRepo;
