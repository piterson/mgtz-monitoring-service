const startServer = require('./server');
const AppRouter = require('./routes');

const router = new AppRouter();

startServer(router, errorHandler())
    .then(() => {
        console.log('Server has been successfully initialized');
    })
    .catch(function(error) {
        console.error(error);
    });


function errorHandler(error, response) {
    if (!error) {
        return;
    }
    if (!error.message) {
        error.message = error.internalError && error.internalError.message
    }
    console.error(error.toString(), error.internalError, error.internalError && error.internalError.message);

    response.status(error.statusCode).json(error);
}
