const Settings = require('common');

class ServerSettings extends Settings {
    constructor() {
        super({
            LISTEN_HOST: '0.0.0.0',
            LISTEN_PORT: 8000,
            CLIENT_DOMAIN: 'http://localhost:4200',
            ALLOWED_SECRETS: '123secretToken456,321secretToken654',
        });
    }

    get allowedSecrets() {
        return this.getValue('ALLOWED_SECRETS').split(',');
    }
    get listenHost() {
        return this.getValue('LISTEN_HOST');
    }
    get listenPort() {
        return this.getValue('LISTEN_PORT');
    }
    get clientDomain() {
        return this.getValue('CLIENT_DOMAIN');
    }
}
module.exports.ServerSettings = ServerSettings;


class DbSettings extends Settings {
    constructor(overrides) {
        super(Object.assign({
            'DB_HOST': undefined,
            'DB_PORT': undefined,
            'DB_USER': undefined,
            'DB_PASS': undefined,
            'DB_DB': undefined,
            'DB_CONNECTION_POOL_SIZE': undefined
        }, overrides));
    }

    get host() {
        return this.getValue('DB_HOST');
    }

    get port() {
        return this.getValue('DB_PORT');
    }

    get user() {
        return this.getValue('DB_USER');
    }

    get password() {
        return this.getValue('DB_PASS');
    }

    get db() {
        return this.getValue('DB_DB');
    }

    get connectionPoolSize() {
        return this.getValue('DB_CONNECTION_POOL_SIZE');
    }
}

exports.DbSettings = DbSettings;
