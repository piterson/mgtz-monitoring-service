-- Table: public.workers

-- DROP TABLE public.workers;

CREATE TABLE public.workers
(
    id SERIAL,
    name character varying(64) COLLATE pg_catalog."default" NOT NULL,
    last_log_id bigint
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;


-- Table: public.worker_logs

-- DROP TABLE public.worker_logs;

CREATE TABLE public.worker_logs
(
    id SERIAL,
    worker_id bigint,
    created timestamp DEFAULT now()
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

-- Table: public.worker_log_data

-- DROP TABLE public.worker_log_data;

CREATE TABLE public.worker_log_data
(
    log_id bigint,
    param_name character varying(64) COLLATE pg_catalog."default" NOT NULL,
    param_value float NOT NULL DEFAULT '0'::float
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;
