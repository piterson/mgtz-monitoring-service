MONITORING_PORT=8000
DB_HOST=localhost
DB_PORT=5432
DB_USER=mgtz
DB_PASS=mgtz
DB_DB=mgtz
DB_CONNECTION_POOL_SIZE=10


psql "dbname='$DB_DB' user='$DB_USER' password='$DB_PASS' host='$DB_HOST'" -f monitoring-service/database-structure.sql

## Run monitoring

cd monitoring-service
yarn

export DB_HOST=$DB_HOST && export DB_PORT=$DB_PORT && export DB_USER=$DB_USER && export DB_PASS=$DB_PASS && export DB_DB=$DB_DB export DB_CONNECTION_POOL_SIZE=$DB_CONNECTION_POOL_SIZE && node src/main.js &

## Run workers

cd ../sample-worker
yarn

## declare an workers set
declare -a workers=("document-worker-1" "document-worker-2" "sms-worker-1" "sms-worker-2" "email-worker-1" "email-worker-2" "email-worker-3")

## now loop through the above array
for i in "${workers[@]}"
do
   echo "$i"
   export ENDPOINT=http://localhost:$MONITORING_PORT/api/worker/stat && export WORKER_ID="$i" && node src/worker.js &
   # or do whatever with individual element of the array
done


## Run client

cd ../monitoring-client
yarn

ng serve
