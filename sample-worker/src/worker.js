/**
 * NodeJS Monitoring Agent Test Script
 */

/**
 * Initialize Monitoring Agent
 */
const MonitoringAgent = require('monitoring-agent');
const MonitoringAgentSettings = require('./settings');
const settings = new MonitoringAgentSettings();

const monitoringAgent = new MonitoringAgent({
	endpoint: settings.endpoint,
	workerId: settings.workerId,
	workerSecret: settings.workerSecret
});

/**
 * Report random metrics every 10 seconds
 */
function testReport() {
	const currentTasks = Math.floor(Math.random() * 100);
	monitoringAgent.pushMetrics({
		tasks: Math.floor(Math.random() * 50),
		cpuUsage: Math.floor(Math.random() * 100), 
		metricXY: Math.floor(Math.random() * 2000)
	});
}
setInterval(testReport, 10000);

