const Settings = require('common');

class MonitoringAgentSettings extends Settings {
    constructor() {
        super({
            ENDPOINT: 'https://my-test-monitoring-system/pushMetrics',
            WORKER_ID: 'test-worker-1',
            WORKER_SECRET: '123secretToken456'
        });
    }

    get endpoint() {
        return this.getValue('ENDPOINT');
    }

    get workerId() {
        return this.getValue('WORKER_ID');
    }

    get workerSecret() {
        return this.getValue('WORKER_SECRET');
    }
}
module.exports = MonitoringAgentSettings;
