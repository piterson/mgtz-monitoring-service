This repository contains of:

* common - library with the common utils/classes
* monitoring-agent - library that pushes worker's statistics to the monitoring service
* monitoring-service - web-service that serves statistics from all workers based on express-app
* sample-worker - some simple tasks that calls `monitoring-agent`

To test everything without deploying docker containers just configure required variables at `run.sh`
then just run this file.
