const axios = require('axios')


class MonitoringAgent {
	constructor({endpoint, workerId, workerSectet}) {
		this.endpoint = endpoint;
		this.workerId = workerId;
		this.workerSectet = workerSectet;
	}

	pushMetrics(params) {
		axios.post(this.endpoint, {workerId: this.workerId, ...params})
		  .catch(function (error) {
		    console.error('Data push was unsuccessfull :(');
		  });
	}
}

module.exports = MonitoringAgent;
